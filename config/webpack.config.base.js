/**
 * Webpack Development Common Configuration.
 */
import path from 'path';
import { execSync } from 'child_process';

import webpack from 'webpack';

import pkg from '../package.json' with { type: "json" };

export default {
    entry: {
        fyj: './index.js'
    },

    output: {
        path: path.resolve(import.meta.dirname, '..', 'dist'),
        filename: '[name].min.js'
    },

    plugins: [
        new webpack.DefinePlugin({
            APP_NAME: `'${pkg.name}'`,
            APP_VERSION: `'${pkg.version}'`,
            COMMIT_HASH: `'${execSync('git rev-parse --short HEAD').toString().trim()}'`
        })
    ],

    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx', '.css', '.scss']
    },

    module: {
        rules: [{
            test: /\.m?js$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                    ]
                }
            }],
            exclude: /node_modules/
        }, {
            test: /\.jsx?$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ]
                }
            }],
            exclude: /node_modules/
        }]
    }
}
